Write-Progress -Activity 'Конвертация' -Status "Иннициализация" -PercentComplete 1
Function Get-FileName($initialDirectory)
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    
    $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenFileDialog.Multiselect = $true
    $OpenFileDialog.initialDirectory = $initialDirectory
    $OpenFileDialog.filter = "PDF (*.pdf)| *.pdf"
    $OpenFileDialog.ShowHelp = $true
    $OpenFileDialog.ShowDialog() | Out-Null
    $OpenFileDialog.filenames
    }

$inputfile = Get-FileName 'c:\Users\%USERNAME%\Desktop\'
$full_path = Get-ChildItem $inputfile | ForEach-Object {$_.Directory.FullName+"\"+ $_.Name}

if (!$full_path) { Exit } 
Write-Progress -Activity 'Конвертация' -Status "Файлы загружены" -PercentComplete 15
sleep 1

foreach ($element in $full_path) {
   
   $new = $element.Replace(".pdf","_converted.pdf") 
   $element = $element.Replace('\','\\')
   $cards_num_on_page = 3
   
   Write-Progress -Activity "Конвертация $element" -Status "Разрезаем PDF на страницы, обрезаем лишнее, сохраняем в JPG" -PercentComplete 25
   sleep 1
   #magick -density 300 $element -trim C:\Script_PDF\temp\out-jpg.jpg
   magick -density 300 $element -trim C:\Script_PDF\temp\out-jpg-%02d.jpg

   Write-Progress -Activity "Конвертация $element" -Status "Получаем параметры страниц" -PercentComplete 35
   sleep 1
   #magick C:\Script_PDF\temp\out-jpg-0.jpg -format %h info:C:\Script_PDF\temp\out1.txt
   #magick C:\Script_PDF\temp\out-jpg-0.jpg -format %w info:C:\Script_PDF\temp\out1_1.txt
   magick C:\Script_PDF\temp\out-jpg-00.jpg -format %h info:C:\Script_PDF\temp\out1.txt
   magick C:\Script_PDF\temp\out-jpg-00.jpg -format %w info:C:\Script_PDF\temp\out1_1.txt

   Write-Progress -Activity "Конвертация $element" -Status "Записываем параметры страниц в файл" -PercentComplete 40
   sleep 1
   $wei = Get-Content C:\Script_PDF\temp\out1_1.txt
   $hei = Get-Content C:\Script_PDF\temp\out1.txt

   Write-Progress -Activity "Конвертация $element" -Status "Вычисляем высоту этикетки" -PercentComplete 45
   sleep 1
   $crop = $hei / $cards_num_on_page + 1 
   $crop_text = $wei.ToString() + 'x' + $crop.ToString()
   
   #magick C:\Script_PDF\temp\out-jpg-*.jpg -fill white -fuzz 20% -opaque "#ebe44e" C:\Script_PDF\temp\out1-jpg.jpg
   #magick C:\Script_PDF\temp\out-jpg-*.jpg -depth 1 -colorspace Gray C:\Script_PDF\temp\out1-jpg.jpg
   #magick C:\Script_PDF\temp\out1-jpg-*.jpg -fill white -fuzz 20% -opaque "#efefef" C:\Script_PDF\temp\out-white-jpg.jpg
   #magick C:\Script_PDF\temp\out-jpg-*.jpg -monochrome -type bilevel  C:\Script_PDF\temp\out1-jpg.jpg
   
   Write-Progress -Activity "Конвертация $element" -Status "Происходит магическая магия" -PercentComplete 60
   magick -density 150 C:\Script_PDF\temp\out-jpg-*.jpg -crop $crop_text +repage C:\Script_PDF\temp\out2-jpg.jpg
   
   #magick C:\Script_PDF\temp\out2-jpg-*.jpg -fuzz 5% -define trim:percent-background=5% -trim C:\Script_PDF\temp\out3-jpg.jpg
   Write-Progress -Activity "Конвертация $element" -Status "Происходит магическая магия (это может быть не быстро)" -PercentComplete 65
   # Основная магия тут
   magick C:\Script_PDF\temp\out2-jpg-*.jpg -bordercolor black -border 1x1 -fuzz 20% -trim -resize 945x756! -quality 80 -bordercolor black -border 2 -bordercolor white -border 30 -resize 630x504! C:\Script_PDF\temp\out3-jpg.jpg
   #magick C:\Script_PDF\temp\out3-jpg-*.jpg  C:\Script_PDF\temp\out4-jpg.jpg
   #magick C:\Script_PDF\temp\out4-jpg-*.jpg  C:\Script_PDF\temp\out5-jpg.jpg
   Write-Progress -Activity "Конвертация $element" -Status "Сохраняем JPG в PDF" -PercentComplete 80
   sleep 1
   magick C:\Script_PDF\temp\out3-jpg-*.jpg $new
   Write-Progress -Activity "Конвертация $element" -Status "Удаляем временные файлы" -PercentComplete 90
   sleep 1
   #Remove-Item C:\Script_PDF\temp\out*
   Write-Progress -Activity "Конвертация $element" -Status "Проверяем не осталось ли файлов для обработки" -PercentComplete 95
   sleep 3
}

Write-Progress -Activity 'Конвертация' -Status "Завершено" -PercentComplete 100

# Сигнал завершения работы скрипта.
[console]::beep(300, 500);
Start-Sleep -s (0.50); 
[console]::beep(300, 500);
Start-Sleep -s (0.50);
[console]::beep(300, 500);
Start-Sleep -s (0.50);
[console]::beep(250, 500);
Start-Sleep -s (0.50);
[console]::beep(350, 250);
[console]::beep(300, 500);
Start-Sleep -s (0.50);
[console]::beep(250, 500);
Start-Sleep -s (0.50);
[console]::beep(350, 250);
[console]::beep(300, 500);
Start-Sleep -s (0.50); 