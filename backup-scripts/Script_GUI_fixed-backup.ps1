Function Get-FileName($initialDirectory)
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    
    $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenFileDialog.Multiselect = $true
    $OpenFileDialog.initialDirectory = $initialDirectory
    $OpenFileDialog.filter = "PDF (*.pdf)| *.pdf"
    $OpenFileDialog.ShowHelp = $true
    $OpenFileDialog.ShowDialog() | Out-Null
    $OpenFileDialog.filenames
    }

$inputfile = Get-FileName 'c:\Users\%USERNAME%\Desktop\'
$full_path = Get-ChildItem $inputfile | ForEach-Object {$_.Directory.FullName+"\"+ $_.Name}

if (!$full_path) { Exit } 


foreach ($element in $full_path) {
   $new = $element.Replace(".pdf","_converted.pdf") 
   $element = $element.Replace('\','\\')
   $cards_num_on_page = 3
   
   magick -density 300 $element -trim C:\Script_PDF\temp\out-jpg.jpg
   magick C:\Script_PDF\temp\out-jpg-0.jpg -format %h info:C:\Script_PDF\temp\out1.txt
   magick C:\Script_PDF\temp\out-jpg-0.jpg -format %w info:C:\Script_PDF\temp\out1_1.txt
   $wei = Get-Content C:\Script_PDF\temp\out1_1.txt
   $hei = Get-Content C:\Script_PDF\temp\out1.txt
   $crop = $hei / $cards_num_on_page + 1 
   $crop_text = $wei.ToString() + 'x' + $crop.ToString()
   
   #magick C:\Script_PDF\temp\out-jpg-*.jpg -fill white -fuzz 20% -opaque "#ebe44e" C:\Script_PDF\temp\out1-jpg.jpg
   magick C:\Script_PDF\temp\out-jpg-*.jpg -depth 1 -colorspace Gray C:\Script_PDF\temp\out1-jpg.jpg
   magick C:\Script_PDF\temp\out1-jpg-*.jpg -fill white -fuzz 20% -opaque "#efefef" C:\Script_PDF\temp\out-white-jpg.jpg
   #magick C:\Script_PDF\temp\out-jpg-*.jpg -monochrome -type bilevel  C:\Script_PDF\temp\out1-jpg.jpg
   
   magick -density 300 C:\Script_PDF\temp\out-white-jpg-*.jpg -crop $crop_text +repage C:\Script_PDF\temp\out2-jpg.jpg
   
   #magick C:\Script_PDF\temp\out2-jpg-*.jpg -fuzz 5% -define trim:percent-background=5% -trim C:\Script_PDF\temp\out3-jpg.jpg
   magick C:\Script_PDF\temp\out2-jpg-*.jpg -bordercolor black -border 1x1 -fuzz 20% -trim -quality 100 C:\Script_PDF\temp\out3-jpg.jpg
   magick C:\Script_PDF\temp\out3-jpg-*.jpg -bordercolor white -border 30 C:\Script_PDF\temp\out4-jpg.jpg
   magick C:\Script_PDF\temp\out4-jpg-*.jpg -resize 1600x1200! C:\Script_PDF\temp\out5-jpg.jpg
   magick C:\Script_PDF\temp\out5-jpg-*.jpg $new
   Remove-Item C:\Script_PDF\temp\out*
}
#no, wait
#where is console?
#ctrl + `