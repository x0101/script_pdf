Function Get-FileName($initialDirectory)
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    
    $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenFileDialog.Multiselect = $true
    $OpenFileDialog.initialDirectory = $initialDirectory
    $OpenFileDialog.filter = "PDF (*.pdf)| *.pdf"
    $OpenFileDialog.ShowHelp = $true
    $OpenFileDialog.ShowDialog() | Out-Null
    $OpenFileDialog.filenames
    }

$inputfile = Get-FileName 'c:\Users\%USERNAME%\Desktop\'
$full_path = Get-ChildItem $inputfile | ForEach-Object {$_.Directory.FullName+"\"+ $_.Name}

if (!$full_path) { Exit } 


foreach ($element in $full_path) {
   $new = $element.Replace(".pdf","_converted.pdf") 
   $element = $element.Replace('\','\\')
   
   magick -density 300 $element  C:\Script_PDF\out-jpg.jpg
   magick C:\Script_PDF\out-jpg-*.jpg -trim  C:\Script_PDF\out1-jpg.jpg

   #magick out1-jpg-0.jpg -format %c histogram:info:C:\Script_PDF\out1.txt
   #$test = Get-Content C:\Script_PDF\out1.txt | findstr '#000000' | %{ $_.Split(':')[0]; }
   #if ( [int]$test -gt 213000) { $crop = 300 } Else { $crop = 333 }
   #magick out1-jpg-*.jpg -crop 675x$crop +repage out2-jpg-%d.jpg
   
   
     
   magick -density 300 C:\Script_PDF\out1-jpg-*.jpg -crop 100%x34% +repage C:\Script_PDF\out2-jpg.jpg
   magick C:\Script_PDF\out2-jpg-*.jpg -trim  C:\Script_PDF\out3-jpg.jpg

   magick C:\Script_PDF\out3-jpg-*.jpg -resize 377x302! -quality 100 C:\Script_PDF\out4-jpg.jpg
   
      
  
   magick C:\Script_PDF\out4-jpg-*.jpg -crop 415x250+15+15 C:\Script_PDF\out5-jpg.jpg

   magick out5-*.jpg $new
   Remove-Item out*
   
   
   #magick C:\Script_PDF\out5-jpg-*.jpg -trim  C:\Script_PDF\out6-jpg.jpg #-undercolor '#ebe44e'  

   #magick C:\Script_PDF\out5-jpg-*.jpg $new
   
   #magick C:\Script_PDF\out5-jpg-*.jpg -crop x700+15+15 C:\Script_PDF\out4-jpg.jpg

   #magick out4-jpg-7.jpg -fill \#ebe44e #\ -draw 'color 30,20 point'      color_point.png

   #magick C:\Script_PDF\out4-jpg-*.jpg $new

   #magick out2-jpg.jpg -format %c histogram:info:C:\Script_PDF\out1.txt
   #$test = Get-Content out1.txt | findstr '#000000' | %{ $_.Split(':')[0]; }
   #if ( [int]$test -gt 213000) { $crop = 300 } Else { $crop = 333 }
   #magick out1-*.jpg -crop 675x$crop +repage out2-%d.jpg
   #magick out2-*.jpg -trim  out3-%d.jpg
   #magick out3-*.jpg -bordercolor white -border 30  out4-%d.jpg
   #magick out4-*.jpg $new
   #Remove-Item out*
}
